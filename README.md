# Project-Dollhouse

A project aiming to rebuild TSO (The Sims Online) from the ground up. Inspired by [CorsixTH](https://github.com/CorsixTH).


# Contributing
They're many ways you can contribute to Project Dollhouse such as trying things out, filing bugs, and joining in the discussion!

* [Getting Started](https://github.com/Afr0Games/Project-Dollhouse/wiki)
* How to Contribute (coming soon)
* [Coding Standards](https://github.com/Afr0Games/Project-Dollhouse/wiki/Coding-standards)
* [Best practices](https://github.com/Afr0Games/Project-Dollhouse/wiki/Best-practices)
* [Pull Requests](https://github.com/Afr0Games/Project-Dollhouse/pulls): [Open](https://github.com/Afr0Games/Project-Dollhouse/pulls)/[Closed](https://github.com/Afr0Games/Project-Dollhouse/issues?q=is%3Apr+is%3Aclosed)
* [Translation](https://github.com/Afr0Games/the-sims-online-translation)
* [Forums](http://forum.afr0games.com)

Looking for something to do? Check to see issues tagged as [good for new contributors](https://github.com/Afr0Games/Project-Dollhouse/labels/good%20for%20new%20contributors) to get started.

# License
> This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
> If a copy of the MPL was not distributed with this file, You can obtain one at
> http://mozilla.org/MPL/2.0/.
